﻿#pragma warning disable CA1416

using System.Drawing.Text;
using System.Drawing;
using System.Text;
using ttf2bmp;
using System.ComponentModel;

namespace ttf2cpp;

internal class ConvertorTtf
{
    public static List<(string, Bitmap)> GenerateBitmaps(
        string strFont,
        string strCharacters,
        int iFontSize,
        Color colorFont,
        Color colorBackground,
        string strSave,
        out bool bCharacters
        )
    {
        //generate the initial bitmaps
        var list = new List<(string, Bitmap)>();
        if(strCharacters.Contains(' '))
        {
            bCharacters = false;
            foreach (var strString in strCharacters.Split(' '))
            {
                list.Add(new(strString, GenerateBitmapOne(strFont, strString, iFontSize, colorFont, colorBackground)));
            }
        }
        else
        {
            bCharacters = true;
            foreach (var character in strCharacters)
            {
                var strCharacter = $"{character}";
                list.Add(new(strCharacter, GenerateBitmapOne(strFont, strCharacter, iFontSize, colorFont, colorBackground)));
            }
        }

        //find wrapper box for all characters
        var iXMin = int.MaxValue;
        var iXMax = int.MinValue;
        var iYMin = int.MaxValue;
        var iYMax = int.MinValue;
        foreach (var item in list)
        {
            FindBox(item.Item2, ref iXMin, ref iXMax, ref iYMin, ref iYMax);
        }
        Console.WriteLine($"Max-box: ({iXMin},{iYMin}) ({iXMax},{iYMax}) --> ({iXMax - iXMin + 1},{iYMax - iYMin + 1})");

        //resize all
        var listResized = new List<(string, Bitmap)>();
        foreach (var item in list)
        {
            //get box for this bmp
            var iXMinItem = int.MaxValue;
            var iXMaxItem = int.MinValue;
            var iYMinItem = int.MaxValue;
            var iYMaxItem = int.MinValue;
            FindBox(item.Item2, ref iXMinItem, ref iXMaxItem, ref iYMinItem, ref iYMaxItem);
            Console.WriteLine($"Box [{item.Item1}]: ({iXMinItem},{iYMin}) ({iXMaxItem},{iYMax}) --> ({iXMaxItem - iXMinItem + 1},{iYMax - iYMin + 1})");

            //calculate center
            var iXDelta = iXMax - iXMin;
            var iXDeltaItem = iXMaxItem - iXMinItem;
            var iXShift = (iXDelta - iXDeltaItem) / 2;

            //create the bitmap
            var bmpResized = Bitmap(iXMax - iXMin + 1, iYMax - iYMin + 1, colorBackground);

            //resize the image
            for (var x = iXMinItem; x <= iXMaxItem; ++x)
            {
                for (var y = iYMin; y <= iYMax; ++y)
                {
                    bmpResized.SetPixel(x - iXMinItem + iXShift, y - iYMin, item.Item2.GetPixel(x, y));
                }
            }
            listResized.Add(new(item.Item1, bmpResized));
        }

        //save all
        foreach (var item in listResized)
        {
            var strFile = strSave + "-" + item.Item1 + "-" + (int)item.Item1[0] + ".bmp";
            try
            {
                if (File.Exists(strFile))
                {
                    File.Delete(strFile);
                }
                item.Item2.Save(strFile, System.Drawing.Imaging.ImageFormat.Bmp);
            }
            catch (Exception)
            {
                strFile = strSave + "-" + (int)item.Item1[0] + ".bmp";
                try
                {
                    if (File.Exists(strFile))
                    {
                        File.Delete(strFile);
                    }
                    item.Item2.Save(strFile, System.Drawing.Imaging.ImageFormat.Bmp);
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"Save [{strFile}] failed: {ex.Message}");
                }
            }
        }

        //all OK
        return listResized;
    }

    private static void FindBox(Bitmap bmp, ref int iXMin, ref int iXMax, ref int iYMin, ref int iYMax)
    {
        var colorBack = bmp.GetPixel(0, 0);
        for (var x = 0; x < bmp.Width; ++x)
        {
            for (var y = 0; y < bmp.Height; ++y)
            {
                var pixel = bmp.GetPixel(x, y);
                if (pixel != colorBack)
                {
                    if (iXMin > x)
                    {
                        iXMin = x;
                    }
                    if (iXMax < x)
                    {
                        iXMax = x;
                    }
                    if (iYMin > y)
                    {
                        iYMin = y;
                    }
                    if (iYMax < y)
                    {
                        iYMax = y;
                    }
                }
            }
        }
    }

    private static Bitmap GenerateBitmapOne(
        string strFont,
        string strCharacter,
        int iFontSize,
        Color colorFont,
        Color colorBackground
        )
    {
        //generate the bmp
        return ConvertorTtf.ToBitmap(
            strFont,
            strCharacter,
            iFontSize * (strCharacter.Length + 1) * 3,
            iFontSize,
            colorFont,
            colorBackground
            );
    }

    private static Bitmap ToBitmap(
        string fontFileName,
        string stringToDraw,
        int iSize,
        int iFontSize,
        Color colorFont,
        Color colorBackground
        )
    {

        //create font 
        var fonts = new PrivateFontCollection();
        fonts.AddFontFile(fontFileName);

        //get font family 
        var family = (FontFamily?)fonts.Families.GetValue(0);
        if (null == family)
        {
            throw new Exception("could not get font family");
        }

        //create bitmap 
        var bitmap = Bitmap(iSize, iSize, colorBackground);

        //create graphics from image 
        Graphics g = Graphics.FromImage(bitmap);

        //draw string 
        g.DrawString(
            stringToDraw,
            new Font(family, iFontSize, FontStyle.Regular),
            new SolidBrush(colorFont),
            0, 0
            );

        return bitmap;

    }

    private static Bitmap Bitmap(int iWidth, int iHeight, Color color)
    {
        var bitmap = new Bitmap(iWidth, iHeight);
        for (var x = 0; x < bitmap.Width; ++x)
        {
            for (var y = 0; y < bitmap.Height; ++y)
            {
                bitmap.SetPixel(x, y, color);
            }
        }
        return bitmap;
    }
}
