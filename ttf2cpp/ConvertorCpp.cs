﻿#pragma warning disable CA1416

using System.Drawing;
using System.Drawing.Text;
using System.Text;

namespace ttf2bmp;

internal class ConvertorCpp
{
    public static void ToCPlusPlus(List<(string, Bitmap)> list, string strSave, bool bCharacters, string strNamespace)
    {
        //create stringbuilder
        var sb = new StringBuilder();

        //pre-guard
        sb.Append($"#ifndef __GENERATED_FONT__{strNamespace.ToUpper()}__H__\n");
        sb.Append($"#define __GENERATED_FONT__{strNamespace.ToUpper()}__H__\n");
        sb.Append('\n');

        //pre-namespace
        sb.Append($"namespace {strNamespace} {{\n");
        sb.Append('\n');

        //add defines
        if (bCharacters)
        {
            sb.Append($"//define NULL\n");
            sb.Append($"#ifndef NULL\n");
            sb.Append($"#define NULL 0\n");
            sb.Append($"#endif //#ifndef NULL\n");
            sb.Append('\n');
        }

        //add typedefs
        sb.Append($"//typedef of the type used for 1 character\n");
        sb.Append($"typedef const int CharacterType[{list[0].Item2.Width}][{list[0].Item2.Height}][3];\n");
        sb.Append('\n');

        //add character dimensions
        sb.Append($"const int iCharacterWidth  = {list[0].Item2.Width};\n");
        sb.Append('\n');
        sb.Append($"const int iCharacterHeight = {list[0].Item2.Height};\n");
        sb.Append('\n');

        //generate
        foreach (var item in list)
        {
            sb.Append(CharacterToCPlusPlus(item.Item1, item.Item2));
            sb.Append('\n');
        }

        //generate an array to acces all characters by ascii code as index
        if(bCharacters)
        {
            var astrVariables = new string[255];
            for (int i = 0; i < astrVariables.Length; i++)
            {
                astrVariables[i] = "NULL";
            }
            foreach (var item in list)
            {
                astrVariables[(int)item.Item1[0]] = "&" + GetCPPName(item.Item1);
            }
            sb.Append($"const CharacterType* apCharacters[{astrVariables.Length}] = {{");
            for (int i = 0; i < astrVariables.Length; i++)
            {
                sb.Append(astrVariables[i]);
                if (i != (astrVariables.Length - 1))
                {
                    sb.Append(@", ");
                }
            }
            sb.Append("};\n");
            sb.Append('\n');

            //usage example
            sb.Append("//usage example:\n");
            sb.Append("//CharacterType* pCharacter = apCharacters[ascii]\n");
            sb.Append("//const int r = (*pCharacter)[x][y][0];\n");
            sb.Append("//const int g = (*pCharacter)[x][y][1];\n");
            sb.Append("//const int b = (*pCharacter)[x][y][2];\n");
            sb.Append('\n');
        }

        //post-namespace
        sb.Append($"}}; //namespace {strNamespace}\n");
        sb.Append('\n');

        //post-guard
        sb.Append($"#endif //#ifndef __GENERATED_FONT__{strNamespace.ToUpper()}__H__\n");

        //write to file
        var strFile = strSave + ".h";
        if(File.Exists(strFile))
        {
            File.Delete(strFile);
        }
        File.WriteAllText(strFile, sb.ToString());
    }
    private static string CharacterToCPlusPlus(string strCharacter, Bitmap bitmap)
    {
        var sb = new StringBuilder();
        sb.Append($"// character '{strCharacter}' ({bitmap.Width},{bitmap.Height})\n");
        sb.Append($"CharacterType {GetCPPName(strCharacter)} = {{");
        for(var x = 0; x < bitmap.Width; ++x)
        {
            for(var y = 0; y < bitmap.Height; ++y)
            {
                var pixel = bitmap.GetPixel(x, y);
                sb.Append(pixel.R);
                sb.Append(", ");
                sb.Append(pixel.G);
                sb.Append(", ");
                sb.Append(pixel.B);
                sb.Append(", ");
            }
        }
        sb.Append('}');
        sb.Append(';');
        sb.Append('\n');
        return sb.ToString().Replace(@", }", @"}");
    }

    private static string GetCPPName(string strCharacter)
    {
        return $"charBmp_{(int)strCharacter[0]}";
    }

}
