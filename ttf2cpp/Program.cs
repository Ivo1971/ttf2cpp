﻿using System.Drawing;
using ttf2bmp;
using ttf2cpp;

//example characters: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijlkmnopqrstuvwxyz1234567890+-=!()_&,*/?;:~@#'§{}$"
//example strings: "OK Clear"

Console.WriteLine("TTTF to C++ font converter");
Console.WriteLine();
Console.WriteLine($"Font file: {args[0]}");
Console.WriteLine($"Out file prefix: {args[1]}");
Console.WriteLine($"Characters: {args[2]}");
Console.WriteLine($"Character size: {int.Parse(args[3])}");
Console.WriteLine($"BMP font color: {args[4]}");
Console.WriteLine($"BMP background color: {args[5]}");
Console.WriteLine();
Console.WriteLine("Generating...");

var list = ConvertorTtf.GenerateBitmaps(
    args[0],
    args[2],
    int.Parse(args[3]),
    Color.FromName(args[4]),
    Color.FromName(args[5]),
    args[1],
    out bool bCharacters
    );
ConvertorCpp.ToCPlusPlus(
    list, 
    args[1],
    bCharacters,
    "bmp" + (bCharacters ? "characters" : "strings")
    );

Console.WriteLine("TTTF to C++ font converter done.");
